<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $casts = [
        'items' => 'array',
    ];

    public function discounts()
    {
        return $this->hasMany('App\Models\Discount');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

}
