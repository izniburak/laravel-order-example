<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'revenue' => 'decimal:2',
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
