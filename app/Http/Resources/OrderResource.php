<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->customer->name,
            'total' => (float) $this->total,
            'items' => $this->items,
            'created_at' => now()->setTimeFromTimeString($this->created_at)->format('d-m-Y H:i:s'),
        ];
    }
}
