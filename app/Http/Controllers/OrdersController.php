<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Services\OrderService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(OrderService $orderService)
    {
        return OrderResource::collection($orderService->getAll());
    }

    public function create(OrderService $orderService, ProductService $productService, Request $request)
    {
        $data = $request->validate([
            'customer_id' => ['required', 'integer', 'exists:customers,id'],
            'items' => ['required', 'array', 'filled'],
            'items.*.product_id' => ['required', 'integer', 'exists:products,id'],
            'items.*.quantity' => ['required', 'integer'],
        ]);

        try {
            $order = $orderService->newOrder($data['customer_id'], $data['items']);
            if ($order) {
                // [TODO] - Move this operation to Queue
                $productService->updateProductStocksForOrder($order);

                return response()->json([
                    'status' => true,
                    'message' => "Order was created successfully."
                ])->setStatusCode(Response::HTTP_BAD_REQUEST);
            }

            return response()->json([
                'status' => false,
                'message' => "Order couldn't be created. Please try again."
            ])->setStatusCode(Response::HTTP_BAD_REQUEST);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ])->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function discount(OrderService $orderService, Request $request)
    {
        $data = $request->validate([
            'order_id' => ['required', 'integer', 'exists:orders,id'],
        ]);

        $discounts = $orderService->calculateDiscount($data['order_id']);

        return response()->json([
            'success' => true,
            'data' => array_merge(
                ['order_id' => $data['order_id']],
                $discounts
            ),
        ]);
    }
}
