<?php

namespace App\Http\Controllers;

use App\Services\OrderService;
use Illuminate\Http\Request;

class DiscountsController extends Controller
{
    public function index(OrderService $orderService, Request $request)
    {
        $data = $request->validate([
            'order_id' => ['required', 'integer', 'exists:orders,id'],
        ]);

        $discounts = $orderService->calculateDiscount($data['order_id']);

        return response()->json([
            'success' => true,
            'data' => array_merge(
                ['order_id' => $data['order_id']],
                $discounts
            ),
        ]);
    }
}
