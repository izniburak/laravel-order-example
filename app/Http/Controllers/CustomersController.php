<?php

namespace App\Http\Controllers;

use App\Http\Resources\CustomerResource;
use App\Http\Resources\OrderResource;
use App\Services\CustomerService;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CustomerService $customer)
    {
        return response()->json([
            'success' => true,
            'data' => CustomerResource::collection($customer->getAll()),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CustomerService $customer, $id)
    {
        return response()->json([
            'success' => true,
            'data' => new CustomerResource($customer->get($id)),
        ]);
    }

    public function orders(CustomerService $customer, $id)
    {
        return response()->json([
            'success' => true,
            'data' => OrderResource::collection($customer->getOrders($id)),
        ]);
    }
}
