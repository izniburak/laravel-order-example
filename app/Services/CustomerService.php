<?php

namespace App\Services;

use App\Models\Customer;

class CustomerService
{
    public function getAll()
    {
        return Customer::all();
    }

    public function get(int $id)
    {
        return Customer::findOrFail($id);
    }

    public function getOrders(int $customerId)
    {
        return $this->get($customerId)->orders()->get();
    }
}
