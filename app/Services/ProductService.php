<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Product;

class ProductService
{
    public function getAll()
    {
        return Product::all();
    }

    public function get(int $id)
    {
        return Product::findOrFail($id);
    }

    public function updateProductStocksForOrder(Order $order): bool
    {
        foreach ($order->items as $item) {
            $product = $this->get($item['product_id']);
            $product->stock = $product->stock - (int)$item['quantity'];
            $product->save();
        }

        return true;
    }
}
