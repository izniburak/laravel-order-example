<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Product;

class DiscountService
{
    private ProductService $productService;

    private array $discounts = [];

    private float $totalDiscount = 0;

    private float $discountedTotal = 0;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function calculate(Order $order)
    {
        $this->discountedTotal = $order->total;

        // Calculate 10_PERCENT_OVER_1000
        $this->calculate10PercentOver1000($order->total);

        $cheapProduct = null;
        foreach ($order->items as $item) {
            $product = $this->productService->get($item['product_id']);

            // Calculate BUY_6_GET_1
            $this->calculateBuy6Get1($product, $item['quantity']);

            // Check 20_PERCENT_FOR_CHEAP
            $cheapProduct = $this->check20PercentForCheap($product, $item['quantity'], $cheapProduct);
        }

        // Calculate 20_PERCENT_FOR_CHEAP
        $this->calculate20PercentForCheap($cheapProduct);

        return [
            'discounts' => $this->discounts,
            'discounted_total' => $this->discountedTotal,
            'total_discount' => $this->totalDiscount,
        ];
    }

    protected function calculate10PercentOver1000(float $totalPrice)
    {
        if ($totalPrice >= 1000) {
            $discountAmount = $this->number($totalPrice * 0.1);
            $this->discountedTotal -= $discountAmount;
            $this->discounts[] = [
                'reason' => '10_PERCENT_OVER_1000',
                'discountAmount' => $discountAmount,
                'subtotal' => $this->discountedTotal,
            ];
            $this->totalDiscount += $discountAmount;
        }
    }

    protected function calculateBuy6Get1(Product $product, int $quantity)
    {
        if ($product->category_id === 2 && ($count = $quantity % 6) > 0) {
            $discountAmount = $this->number($product->price * $count);
            $this->discountedTotal -= $discountAmount;
            $this->discounts[] = [
                'reason' => 'BUY_6_GET_1',
                'discountAmount' =>  $discountAmount,
                'subtotal' => $this->discountedTotal,
            ];
            $this->totalDiscount += $discountAmount;
        }
    }

    protected function check20PercentForCheap(Product $product, int $quantity, ?Product $cheapProduct)
    {
        if ($product->category_id === 1 && $quantity > 1) {
            if (is_null($cheapProduct) || $product->price < $cheapProduct->price) {
                return $product;
            }
        }

        return null;
    }

    protected function calculate20PercentForCheap($cheapProduct)
    {
        if ($cheapProduct) {
            $discountAmount = $this->number($cheapProduct->price * 0.2);
            $this->discountedTotal -= $discountAmount;
            $this->discounts[] = [
                'reason' => '20_PERCENT_FOR_CHEAP',
                'discountAmount' =>  $discountAmount,
                'subtotal' => $this->discountedTotal,
            ];
            $this->totalDiscount += $this->number($discountAmount);
        }
    }

    protected function number($number): float
    {
        return (float) number_format($number, 2);
    }
}
