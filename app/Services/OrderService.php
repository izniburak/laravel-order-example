<?php

namespace App\Services;

use App\Models\Order;

class OrderService
{
    private CustomerService $customerService;
    private ProductService $productService;
    private DiscountService $discountService;

    public function __construct(CustomerService $customerService, ProductService $productService, DiscountService $discountService)
    {
        $this->customerService = $customerService;
        $this->productService = $productService;
        $this->discountService = $discountService;
    }

    public function getAll()
    {
        return Order::all();
    }

    public function get(int $id)
    {
        return Order::findOrFail($id);
    }

    public function newOrder(int $customerId, array $items)
    {
        $customer = $this->customerService->get($customerId);

        $orderList = [];
        $total = 0;
        foreach ($items as $item) {
            $product = $this->productService->get($item['product_id']);

            if ($item['quantity'] > $product->stock) {
                throw new \Exception("There is no enough stock for the Product [{$product->name}]");
            }

            $totalPrice = $product->price * $item['quantity'];
            $orderList[] = [
                'product_id' => $product->id,
                'quantity' => $item['quantity'],
                'unit_price' => $product->price,
                'total' => $totalPrice,
            ];

            $total += $totalPrice;
        }

        $order = new Order;
        $order->customer_id = $customer->id;
        $order->items = $orderList;
        $order->total = $total;

        if ($order->save()) {
            return $order;
        }

        return false;
    }

    public function calculateDiscount(int $orderId): array
    {
        $order = $this->get($orderId);

        $discounts = $this->discountService->calculate($order);

        return $discounts;
    }
}
