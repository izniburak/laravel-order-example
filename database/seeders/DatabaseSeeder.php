<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Products
        $products = json_decode(file_get_contents(base_path('example-data/products.json')));

        foreach ($products as $product) {
            Product::create([
                'id' => $product->id,
                'name' => $product->name,
                'category_id' => $product->category,
                'price' => $product->price,
                'stock' => $product->stock,
            ]);
        }

        // Customers
        $customers = json_decode(file_get_contents(base_path('example-data/customers.json')));
        foreach ($customers as $customer) {
            Customer::create([
                'id' => $customer->id,
                'name' => $customer->name,
                'since' => $customer->since,
                'revenue' => $customer->revenue,
            ]);
        }

        // Orders
        $orders = json_decode(file_get_contents(base_path('example-data/orders.json')));
        foreach ($orders as $order) {
            $items = [];
            $total = 0;
            foreach ($order->items as $product) {
                $price = (float) $product->unitPrice;
                $totalPrice = $price * $product->quantity;
                $items[] = [
                    'product_id' => $product->productId,
                    'quantity' => $product->quantity,
                    'unit_price' => (float) number_format($price, 2),
                    'total' => $totalPrice,
                ];
                $total += $totalPrice;
            }

            Order::create([
                'id' => $order->id,
                'customer_id' => $order->customerId,
                'items' => $items,
                'total' => $total,
            ]);
        }

        // Discounts
        $discount = json_decode(file_get_contents(base_path('example-data/discount.response.json')));

        foreach ($discount->discounts as $dis) {
            Discount::create([
                'order_id' => $discount->orderId,
                'reason' => $dis->discountReason,
                'amount' => $dis->discountAmount,
            ]);
        }

    }
}
