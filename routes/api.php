<?php

use App\Http\Controllers\CustomersController;
use App\Http\Controllers\DiscountsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('products', ProductsController::class);
Route::resource('customers', CustomersController::class);
Route::get('customers/{id}/orders', [CustomersController::class, 'orders']);
Route::get('orders', [OrdersController::class, 'index']);
Route::post('orders/make', [OrdersController::class, 'create']);
Route::post('discounts', [DiscountsController::class, 'index']);
