## Example Laravel Project

Before running the Project, you must install **Docker** to your computer. After the installation of the Docker, you can follow the instructions below.

---
I created this project with Laravel Sail. So, you should use `Sail` commands in order to run the project. 

Firstly, go to the Project path and create `.env` file:
```sh
$ cp .env.local .env
```

After that, let's run the project on Docker:
```sh
$ ./vendor/bin/sail up 
```

Then, run the migrations and fill the database via default seeder:
```sh
$ ./vendor/bin/sail artisan migrate:fresh
```
```sh
$ ./vendor/bin/sail artisan db:seed
```
And now, Project is ready. You can check whether project is running or not on http://localhost.

You can use following endpoints for the Project:

- GET /api/customers
- GET /api/customers/{id}
- GET /api/customers/{id}/orders
- GET /api/orders
- POST /api/orders/make
- POST /api/discounts
- GET /api/products
- GET /api/products/{id}
